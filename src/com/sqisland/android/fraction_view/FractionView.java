package com.sqisland.android.fraction_view;

import android.R.drawable;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class FractionView extends View{
	private Paint mCirclePaint = null;
	private Paint mCirclePaint1 = null;

	private Paint mSectorPaint;
	private RectF mSectorOval;
	private int mNumerator;
	private int mDenominator;
    private int radius;
    private int cx;
    private int cy;
	public FractionView(Context context) {
		super(context);
		init();
	}

	public FractionView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();


	}

	public FractionView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {

		mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mCirclePaint.setColor(Color.RED);//fill it with cyan
		mCirclePaint.setStyle(Paint.Style.FILL);
		setBackgroundColor(Color.LTGRAY);

		mSectorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mSectorPaint.setColor(Color.WHITE);//fill it with cyan
		mSectorPaint.setStyle(Paint.Style.FILL);
		mSectorOval = new RectF(0, 0, 100, 100);
		mNumerator = 1;
		mDenominator = 5;


		//Square self
		mCirclePaint1 = new Paint(Paint.ANTI_ALIAS_FLAG);

		mCirclePaint1.setColor(Color.YELLOW);
		mCirclePaint1.setStyle(Paint.Style.FILL_AND_STROKE);
	}

	protected void onDraw(Canvas canvas) {
		//1)
		/*  int cx = 30;
	  int cy = 30;
	  int radius = 20;
	  canvas.drawCircle(cx, cy, radius, mCirclePaint);*/

		int width = getWidth() - getPaddingLeft() - getPaddingRight();
		int height = getHeight() - getPaddingTop() - getPaddingBottom();
		int size = Math.min(width, height);
		cx = width / 2 + getPaddingLeft();
		cy = height / 2 + getPaddingTop();
		radius = size / 2;
		canvas.drawCircle(cx, cy, radius, mCirclePaint);




		//in ondraw function we are operating in pixel . if you have 180 dp, but if the device has double density,
		//we can get the 360dp.
		// 3) //canvas.drawArc(mSectorOval, 0, 60, true, mSectorPaint);

		mSectorOval.top = (height - size) / 2 + getPaddingTop();
		mSectorOval.left = (width - size) / 2 + getPaddingLeft();
		mSectorOval.bottom = mSectorOval.top + size;
		mSectorOval.right = mSectorOval.left + size;
		canvas.drawArc(mSectorOval, 0, getSweepAngle(), true, mSectorPaint);

		canvas.drawRect(getWidth() - 40, getHeight() - 40, getWidth(), getHeight(), mCirclePaint1);
		canvas.drawRect(0, 0, 40, 40, mCirclePaint1);
		canvas.drawRect(getWidth() - 40, 0, getWidth(), 40, mCirclePaint1);
		canvas.drawRect(0, getHeight() - 40, 40 , getHeight(), mCirclePaint1);

	}

	private float getSweepAngle() {
		return mNumerator * 360f / mDenominator;
	}

	public void setFraction(int numerator, int denominator){
		mNumerator = numerator;
		mDenominator = denominator;
		invalidate();
		
		if (mListener != null) {
			mListener.onChange(numerator, denominator);
		}
	}


	public boolean onTouchEvent(MotionEvent event) {
		 //Region region = new Region(left, top, right, bottom);//---click inside the circle to change the part of color in circle

		   int x = (int) event.getX();
		   int y = (int) event.getY();

		   //if ((x < cx + radius * cos(a)) && x > (cx - radius * cos(a)) && y > (cy - radius * sin(a) ) && y < (cy + radius * sin(a)))
		   if( (((x - cx) * (x - cx)) + ((y-cy) *(y-cy)) ) < (radius * radius))
		   {
				if (event.getAction() != MotionEvent.ACTION_UP) {
					return true;
				}
				// Increment the numerator, cycling back to 0 when we have filled the
				// whole circle.
				int numerator = mNumerator + 1;
				if (numerator > mDenominator) {
					numerator = 0;
				}
				setFraction(numerator, mDenominator);
				
		   }
		   return true;
	
	}
	//Change view when finger is lifted


	public Parcelable onSaveInstanceState() {
		Bundle bundle = new Bundle();
		bundle.putParcelable("superState", super.onSaveInstanceState());
		bundle.putInt("numerator", mNumerator);
		bundle.putInt("denominator", mDenominator);
		return bundle;
	}

	public void onRestoreInstanceState(Parcelable state) {
		if (state instanceof Bundle) {
			Bundle bundle = (Bundle) state;
			mNumerator = bundle.getInt("numerator");
			mDenominator = bundle.getInt("denominator");
			super.onRestoreInstanceState(bundle.getParcelable("superState"));
		} else {
			super.onRestoreInstanceState(state);
		}
		setFraction(mNumerator, mDenominator);
		invalidate();
	}


	public interface OnChangeListener {
		public void onChange(int numerator, int denominator);
	}
	
	private OnChangeListener mListener = null;
	public void setOnChangeListener(OnChangeListener listener) {
		mListener = listener;
	}
}