package com.sqisland.android.fraction_view;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.app.Activity;

public class MainActivity extends Activity{
	private Button btClickMe;
	FractionView mFractionView; 
	TextView tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mFractionView = (FractionView)findViewById(R.id.vFract);
		mFractionView.setFraction(1, 5);

		tv = (TextView)findViewById(R.id.tv);


		mFractionView.setOnChangeListener(new FractionView.OnChangeListener() {
			public void onChange(int numerator, int denominator) {
				String fraction = getString(R.string.fraction, numerator, denominator);
				tv.setText(fraction);
			}
		});
	}
}



